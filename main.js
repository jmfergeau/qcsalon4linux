const { app, BrowserWindow } = require('electron');
const path = require('path');

// THIS METHOD IS COMPLETE BS AND YOU SHOULD FEEL BAD, ELECTRON!
let dirName = __dirname
if (app.isPackaged) dirName = path.join(process.resourcesPath)

if (process.platform === "win32") {
  iconPath = path.join(dirName, "build", "icons", "icon.ico")
} else if (process.platform === "linux") {
  iconPath = path.join(dirName, "build", "icons", "icon-256x256.png")
} else if (process.platform === "darwin") {
  iconPath = path.join(dirName, "build", "icons", "icon.icns")
}

const options = {
  title: app.name,
  width: 1024,
  height: 600,
  icon: iconPath,
  autoHideMenuBar: true,
  webPreferences: {
    nativeWindowOpen: true,
    nodeIntegration: true
  }
}
/* 
if (process.platform === "linux") {
  options.icon = path.join(`${__dirname}/icons/icon.png`);
} */

const createWindow = () => {
  const win = new BrowserWindow(options);

  win.loadURL("https://qcsalon.net/fr/game");
  
  let contents = win.webContents

  contents.insertCSS(`
    body {
      overflow: hidden;
    }
    #app {
      padding: 0.5em;
    }
  `)
}

app.whenReady().then(() => {
  createWindow()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})
