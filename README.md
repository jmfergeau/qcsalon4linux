# Unofficial QCSalon app for Linux

QCSalon had a windows native version but nothing for mac and linux. While the windows app can be used on wine, i was wondering if i could make this app to learn electron a bit more and give a native alternative to that.

Please bring any feedback see if it's easily accessible like the original app !

# Commands
- `npm install` to get the dependencies.
- `npm start` to test the program.
- `npm run build` to build the Appimage.

